# README #

This is the repository used as a template for git-related exercises.

### What is this repository for? ###

This repository contains a simple calculator example written in Java.

### How do I get set up? ###

Open the project using your favourite IDE e.g. Netbeans, Eclipse or IntelliJ IDEA.

Compile your project using Maven Goals: 
* clean test jacoco:report org.pitest:pitest-maven:mutationCoverage

### Contribution guidelines ###

### Who do I talk to? ###

* Nuno Pereira (nap@isep.ipp.pt)